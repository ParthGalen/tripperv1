var map = new GMaps({
  el: '#map',
  lat:  52.247656,
  lng: 21.014563,
  zoom: 12
});

var tripContent = $('#tripContent');

$('#tripFile').change(function(){
	var fd = new FormData();    
	fd.append('tripFile', $(this)[0].files[0]);

	$.ajax({
		url: 'http://tripper-api.azurewebsites.net/trips',
		data: fd,
		processData: false,
		contentType: false,
		type: 'POST',
		success: function(data){
			$('#tripContent li').remove();
			if(tripContent.is(':visible'))
				tripContent.hide();

			$('#errMsg div').remove();
			$('#successMsg div').remove();
			if(data.errMsg){
				$('#errMsg').append('<div class="alert alert-dismissible alert-danger">' + data.errMsg + '</div>');
				return;
			}
			
			var tripId = data.id;	

			$.ajax({
				url: 'http://tripper-api.azurewebsites.net/trips/' + tripId,
				success: function(data){
					console.log(data);

					var tripData = JSON.parse(data.tripData);
					var features = tripData.features;
					var flen = features.length;
					var idx = 0;
					for(var i = 0; i < flen; i++){
						if(features[i].geometry.type == 'LineString'){
							if(i == 0)
								map = new GMaps({
									el: '#map',
									lat: features[0].geometry.coordinates[0][1],
									lng: features[0].geometry.coordinates[0][0],
									zoom: 10
								});
							var clen = features[i].geometry.coordinates.length;
							for(var j = 0; j < clen; j++){
								map.addMarker({
									lat: features[i].geometry.coordinates[j][1],
									lng: features[i].geometry.coordinates[j][0],
									m_id: idx++,
									infoWindow: {
										content: '<p>point ' + i + '.' + j + '<br>select image: <input type="file" id="tripImg" name="tripImg" /></p>'
									}
								});
							}
						}
						if(features[i].geometry.type == 'Point'){
							if(i == 0)
								map = new GMaps({
									el: '#map',
									lat: features[0].geometry.coordinates[1],
									lng: features[0].geometry.coordinates[0],
									zoom: 10
								});
							map.addMarker({
								lat: features[i].geometry.coordinates[1],
								lng: features[i].geometry.coordinates[0],
								m_id: idx++,
								infoWindow: {
									content: '<p>point ' + i + '<br>select image: <input type="file" id="tripImg" name="tripImg" /></p>'
								}
							});
						}
						if(features[i].geometry.type == 'MultiLineString'){
							if(i == 0)
								map = new GMaps({
									el: '#map',
									lat: features[0].geometry.coordinates[0][0][1],
									lng: features[0].geometry.coordinates[0][0][0],
									zoom: 10
								});
							var clen = features[i].geometry.coordinates.length;
							for(var j = 0; j < clen; j++){
								var zlen = features[i].geometry.coordinates[j].length;
								for(var z = 0; z < zlen; z++){
									map.addMarker({
										lat: features[i].geometry.coordinates[j][z][1],
										lng: features[i].geometry.coordinates[j][z][0],
										m_id: idx++,
										infoWindow: {
											content: '<p>point ' + i + '.' + j + '<br>select image: <input type="file" id="tripImg" name="tripImg" /></p>'
										}
									});
								}
							}
						}
						
						if(features[i].properties.name){
							if(features[i].properties.desc)
								$('#tripContent ul').append('<li class="list-group-item">name: ' + features[i].properties.name + ', description: ' + features[i].properties.desc + '</li>');
							else
								$('#tripContent ul').append('<li class="list-group-item">name: ' + features[i].properties.name + '</li>');
							
							if(tripContent.is(':hidden'))
								tripContent.slideDown('fast');
						}
							
					}
				},
				error: function(data){
					console.log(data);
				},
				complete: function(){
					for(var i = 0; i < map.markers.length; i++){
						map.markers[i].addListener('click', function(){
							// var lat = this.getPosition().lat();
							// var lng = this.getPosition().lng();
							var id = this.get('m_id');
							console.log('marker id: ' + id);
							$('#tripImg').change(function(){
								var fd = new FormData();    
								fd.append('mediaFile', $(this)[0].files[0]);
								fd.append('m_id', id);
								$.ajax({
									url: 'http://tripper-api.azurewebsites.net/trips/' + tripId + '/media',
									data: fd,
									processData: false,
									contentType: false,
									type: 'POST',
									success: function(data){
										$('#errMsg div').remove();
										$('#successMsg div').remove();
										if(data.errMsg)
											$('#errMsg').append('<div class="alert alert-dismissible alert-danger">' + data.errMsg + '</div>');
										else if(data.successMsg)
											$('#successMsg').append('<div class="alert alert-dismissible alert-success">' + data.successMsg + '</div>');

									},
									error: function(data){
										console.log(data);
									}
								});
							});
						});
					}

					cidx = 0;
					GMaps.on('click', map.map, function(evt){
						var lat = evt.latLng.lat();
				    	var lng = evt.latLng.lng();
				    	map.addMarker({
				    		lat: lat,
				    		lng: lng,
				    		cidx: cidx,
				    		icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
				    		infoWindow: {
				    			content: '<p>custom point: ' + cidx + '<br>select image: <input type="file" id="customImg" name="customImg" /><br><input type="button" value="remove" onclick="deleteMarker(' + cidx++ + ')" /></p>'
				    		}
				    	});

						map.markers[map.markers.length - 1].addListener('click', function(){
							$('#customImg').change(function(){
								var fd = new FormData();    
								fd.append('mediaFile', $(this)[0].files[0]);
								fd.append('media', JSON.stringify({ lat: lat, lng: lng }));
								$.ajax({
									url: 'http://tripper-api.azurewebsites.net/trips/' + tripId + '/media2',
									data: fd,
									processData: false,
									contentType: false,
									type: 'POST',
									success: function(data){
										$('#errMsg div').remove();
										$('#successMsg div').remove();
										if(data.errMsg)
											$('#errMsg').append('<div class="alert alert-dismissible alert-danger">' + data.errMsg + '</div>');
										else if(data.successMsg)
											$('#successMsg').append('<div class="alert alert-dismissible alert-success">' + data.successMsg + '</div>');

									},
									error: function(data){
										console.log(data);
									}
								});
							});
						});
					});
				}
			});
		},
		error: function(data){
			$('#errMsg').append('<div class="alert alert-dismissible alert-danger">' + data.statusText + '</div>');
			console.log(data);	
		}
	});
});

function deleteMarker(cidx){
	var len = map.markers.length;
	var tidx;
	while(len--){
		tidx = map.markers[len].get('cidx');
		if(tidx == cidx){
			map.markers[len].setMap(null);
			break;
		}
	}
}