var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var tj = require('@mapbox/togeojson');
var DOMParser = require('xmldom').DOMParser;
var path = require('path');
var multer  = require('multer');

var storage = multer.memoryStorage();
var uploadTrip = multer({ storage: storage }).single('tripFile');
var uploadImg = multer({ storage: storage }).single('tripImg');
var uploadCustom = multer({ storage: storage }).single('mediaFile');

app.use(express.static(__dirname + '/public'));

app.get('/', function(req,res) {
    res.sendFile(path.join(__dirname + '/public/app/index.html'));
});

app.post('/trips/upload', uploadTrip, function (req, res, next) {
	var tripFile = req.file;
	var suffix = tripFile.originalname.slice(-3);

	if(suffix == 'kml' || suffix == 'gpx'){
		var dom = new DOMParser().parseFromString(tripFile.buffer.toString('utf-8'));
		var converted;
		if(suffix == 'kml')
			converted = tj.kml(dom);
		else
			converted = tj.gpx(dom);
		res.json(converted);
	}
	else
		res.json({ errMsg: 'wrong file format' });
});	

app.post('/trip/upload_media', uploadImg, function (req,res) {
    var tripImg = req.file;
    var m_id = req.body.m_id;
    var suffix = tripImg.originalname.slice(-3).toLowerCase();

	if(suffix == 'jpg' || suffix == 'png'){
		res.json({ successMsg: 'image with id ' + m_id + ' saved' });
	}
	else
		res.json({ errMsg: 'jpg, png only' });
});

app.post('/trip/upload_custom', uploadCustom, function (req,res) {
    var customImg = req.file;
    var media = JSON.parse(req.body.media);
    var lat = media.lat;
    var lng = media.lng;
    var suffix = customImg.originalname.slice(-3).toLowerCase();

	if(suffix == 'jpg' || suffix == 'png'){
		res.json({ successMsg: 'image with latitude: ' + lat + ', longitude: ' + lng + ' saved' });
	}
	else
		res.json({ errMsg: 'jpg, png only' });
});

app.listen(port, function(){
	console.log('Running the server on port ' + port);
});